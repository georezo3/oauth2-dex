<?php
namespace Georezo\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;
use Georezo\OAuth2\Client\Provider\Exception\DexIdentityProviderException;

class Dex extends AbstractProvider
{
    use BearerAuthorizationTrait;

    protected $customDomain;
    protected $customScheme;
    protected $urlAuthorize;
    protected $urlAccessToken;
    protected $urlResourceOwnerDetails;

    protected function domain()
    {
        if ($this->customDomain !== null) {
            return $this->customDomain;
        }

        $domain = 'localhost:5556/dex';

        return $domain;
    }

    protected function baseUrl()
    {
        if (!$this->customScheme !== null) {
            return  $this->customScheme .'://'. $this->domain();
        } else {
            return 'http://' . $this->domain();
        }
    }

    public function getBaseAuthorizationUrl()
    {
        if (!$this->urlAuthorize !== null) {
            return $this->urlAuthorize;
        } else {
            return $this->baseUrl() . '/auth';
        }
    }

    public function getBaseAccessTokenUrl(array $params = [])
    {
        if (!$this->urlAccessToken !== null) {
            return $this->urlAccessToken;
        } else {
            return $this->baseUrl() . '/token';
        }
    }

    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        if (!$this->urlResourceOwnerDetails !== null) {
            return $this->urlResourceOwnerDetails;
        } else {
            return $this->baseUrl() . '/userinfo';
        }
    }

    public function getDefaultScopes()
    {
        return ['openid', 'email', 'profile'];
    }

    protected function checkResponse(ResponseInterface $response, $data)
    {
        if ($response->getStatusCode() >= 400) {
            return DexIdentityProviderException::fromResponse(
                $response,
                $data['error'] ?: $response->getReasonPhrase()
            );
        }
    }

    protected function createResourceOwner(array $response, AccessToken $token)
    {
        return new DexResourceOwner($response);
    }

    protected function getScopeSeparator()
    {
        return ' ';
    }
}
